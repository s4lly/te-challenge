function createResource(route, params) {
  let resource = {route}
  resource.url = this.createUrl(params)

  return resource
}

function createResources(routes, configStore) {
  return routes.map(route => {
    let {time=0} = configStore[route]

    let params = [
      {key: 'r', val: route},
      {key: 't', val: time},
    ]

    return this.createResource(route, params)
  })
}

/**
 * extend APIFactory with methods to help retrieve vehicle locations
 *
 * @param {APIFactory} apiFactory 
 */
export default function (apiFactory) {
  apiFactory.createResource = createResource
  apiFactory.createResources = createResources

  return apiFactory
}
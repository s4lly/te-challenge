const REDUCER = (acc, cur, i) => acc + `${i==0?'?':'&'}${cur.key}=${cur.val}`

class API {
  constructor(base) {
    this._BASE = base
    this._BASE_PARAMS = []
  }

  resource(resource) {
    this._RESOURCE = resource
    return this
  }

  params(baseParams) {
    this._BASE_PARAMS = baseParams
    return this
  }

  createUrl(params=[]) {
    params = [...this._BASE_PARAMS, ...params]
    return `${this._BASE}/${this._RESOURCE}${params.reduce(REDUCER, '')}`
  }
}

export default {
  base(base) {
    return new API(base)
  }
}
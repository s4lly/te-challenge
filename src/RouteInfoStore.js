import APIFactory from './APIFactory'

const BASE = 'http://webservices.nextbus.com/service'
const RESOURCE = 'publicJSONFeed'
const SF_MUNI_AGENCY_PARAM = {key: 'a', val:'sf-muni'}

const routeListAPI = APIFactory.base(BASE)
  .resource(RESOURCE)
  .params([{key: 'command', val: 'routeList'}, SF_MUNI_AGENCY_PARAM])

const routeInfoAPI = APIFactory.base(BASE)
  .resource(RESOURCE)
  .params([{key: 'command', val: 'routeConfig'}, SF_MUNI_AGENCY_PARAM])

/**
 * fetches route list from NextBus API
 *
 * @returns promise to retrieve route list data
 */
function fetchRouteList() {
  return fetch(routeListAPI.createUrl()).then(res => res.json())
}

function processRouteList(data) {
  this.state.routes = data.route
}

/**
 * fetches the route info from NextBus API
 *
 * @returns promise to retrieve route config data
 */
function fetchRouteInfo() {
  return fetch(routeInfoAPI.createUrl()).then(res => res.json())
}

function processRouteInfo(data) {
  for (let route of data.route) {
    this.state.config[route.tag] = {
      time: undefined,
      color: route.color,
    }
  }
}

export default {
  state: {
    // list of available routes to select from. per reqs only from "sf-muni"
    routes: [],

    // maintain color and time for route
    config: {},
  },

  /**
   * fetches the list of available routes
   *
   * @returns promise to retrieve the route list and route info
   */
  fetchInfo() {
    return (
      fetchRouteList()
      .then(processRouteList.bind(this))

      .then(fetchRouteInfo)
      .then(processRouteInfo.bind(this))
    )
  },
}
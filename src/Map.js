import * as d3 from "d3"

const WIDTH = 800
const HEIGHT = 500

const SF_MAP_CENTER = [-122.4, 37.771]
const SF_NEIGHBORHOODS = 'sfmaps/neighborhoods.json'
const SF_ARTERIES = 'sfmaps/arteries.json'

let projection = d3.geoMercator()
  .scale(180000)
  .center(SF_MAP_CENTER)

let path = d3.geoPath()
  .projection(projection)

function zooming() {
  let offset = [d3.event.transform.x, d3.event.transform.y]
  let newScale = d3.event.transform.k * 2000

  projection.translate(offset)
    .scale(newScale)

  d3.select('#svg-base').selectAll('path')
    .attr('d', path)

  d3.select('#svg-vehicles').selectAll('circle')
    .attr('cx', d => projection([d.lon, d.lat])[0])
    .attr('cy', d => projection([d.lon, d.lat])[1])
}

export default {
  /**
   * Initialize the D3 base elements, including base svg element and groups.
   */
  init() {
    // create base svg element and append groups
    let svg = d3.select('#map-container')
      .append('svg')
        .attr('width', WIDTH)
        .attr('height', HEIGHT)

    let base = svg.append('g')
      .attr('id', 'svg-base')

    base.append('g')
      .attr('id', 'svg-map')
      .style('fill', 'lightgrey')

    base.append('g')
      .attr('id', 'svg-arteries')
      .attr('stroke', 'white')
      .attr('fill', 'transparent')

    svg.append('g')
      .attr('id', 'svg-vehicles')

    // append rect to base group to catch "zoom" events
    base.append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', WIDTH)
      .attr('height', HEIGHT)
      .attr('opacity', 0)

    let zoom = d3.zoom().on('zoom', zooming)
    base
      .call(zoom)
      .call(
        zoom.transform,
        d3.zoomIdentity.translate(WIDTH/2, HEIGHT/2).scale(100)
      )
  },

  /**
   * Builds the base map of San Francisco, including land and roads.
   */
  buildBaseMap() {
    // build "land" map
    d3.json(SF_NEIGHBORHOODS).then(json => {
      d3.select('#svg-map').selectAll('path')
        .data(json.features)
        .enter()
        .append('path')
        .attr('d', path)
    })

    // build "road" map
    d3.json(SF_ARTERIES).then(json => {
      d3.select('#svg-arteries').selectAll('path')
        .data(json.features)
        .enter()
        .append('path')
        .attr('d', path)
    })
  },

  /**
   * draw new vehicle locations and or update existing vehicle locations.
   *
   * @param {String} route the tag of the route being drawn
   * @param {Object[]} vehicleLocations list of vehicle location objects
   * @param {Object} routeConfig configuration data for route being drawn
   */
  drawVehicleLocations(route, vehicleLocations, routeConfig) {
    let vehicleRouteId = `#vehicle-route-${route}`
    let routeContainer = d3.select(vehicleRouteId)
    
    if (routeContainer.empty()) {
      routeContainer = d3.select('#svg-vehicles')
        .append('g')
        .attr('id', vehicleRouteId.slice(1))
    }

    let vehicles = routeContainer.selectAll('circle')
      .data(vehicleLocations, d => d.id)

    vehicles
      .enter()
        .append('circle')
        .attr('cx', d => projection([d.lon, d.lat])[0])
        .attr('cy', d => projection([d.lon, d.lat])[1])
        .attr('r', 0)
      .merge(vehicles)
        .transition()
        .attr('cx', d => projection([d.lon, d.lat])[0])
        .attr('cy', d => projection([d.lon, d.lat])[1])
        .attr('r', 3)
        .style('fill', routeConfig.color)
  },

  removeVehicleLocation(route) {
    d3.select(`#vehicle-route-${route}`).remove()
  }
}
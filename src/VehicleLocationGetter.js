import Map from './Map'
import APIFactory from './APIFactory'
import VehicleLocAPIMixin from './VehicleLocAPIMixin'

const BASE = 'http://webservices.nextbus.com/service'
const RESOURCE = 'publicJSONFeed'
const SF_MUNI_AGENCY_PARAM = {key: 'a', val:'sf-muni'}

let vehicleLocationAPI = APIFactory.base(BASE)
  .resource(RESOURCE)
  .params([{key: 'command', val: 'vehicleLocations'}, SF_MUNI_AGENCY_PARAM])

VehicleLocAPIMixin(vehicleLocationAPI)

export default {
  /**
   * fetches the "vehicle locations" for the "selected routes".
   *
   * @param {String[]} routes list of selected route tags
   * @param {Object[]} configStore configuration data for all routes
   * @returns promise to retrieve vehicle locations for all the selected routes
   */
  fetchData(routes, configStore) {
    // # generate resources for each selected route
    let resources = vehicleLocationAPI.createResources(routes, configStore)

    // # fetch the vehicle locations for each selected route
    return resources.map(resource => {
      return fetch(resource.url)
        .then(res => res.json())
        .then(data => {
          configStore[resource.route].time = data.lastTime.time

          if (!data.vehicle) return

          Map.drawVehicleLocations(
            resource.route, data.vehicle, configStore[resource.route]
          )
        })
    })
  },
}